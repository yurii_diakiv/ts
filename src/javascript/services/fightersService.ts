import Fighter from '../../../models/fighter';
import FighterDetails from '../../../models/fighterDetails';
import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: Fighter[] = <Fighter[]>(await callApi(endpoint, 'GET'));

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<FighterDetails> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;

    //start
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: FighterDetails = <FighterDetails>(await callApi(endpoint, 'GET'));

      return apiResult;
    } catch (error) {
      throw error;
    }
    //end
  }
}

export const fighterService = new FighterService();
