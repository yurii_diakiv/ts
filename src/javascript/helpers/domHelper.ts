export function createElement({ tagName, className, attributes = {} }: { tagName: string, className?: string, attributes?: {[k: string]: string} }) {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
