import FighterDetails from '../../../models/fighterDetails';
import { controls } from '../../constants/controls';

export async function fight(firstFighter: FighterDetails, secondFighter: FighterDetails): Promise<FighterDetails> {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    
    //start
    let keysPressed: {[k: string]: boolean} = {};

    let playerOneCanUseCrit: boolean = true;
    let playerTwoCanUseCrit: boolean = true;

    let firstFighterIndicator: HTMLElement = <HTMLElement>document.getElementById('left-fighter-indicator');
    let secondFighterIndicator: HTMLElement = <HTMLElement>document.getElementById('right-fighter-indicator');
    const firstFighterIndicatorCoef: number = 100 / firstFighter.health;
    const secondFighterIndicatorCoef: number = 100 / secondFighter.health;

    let availableKeys: string[] = Object.values(controls).flat();

    document.addEventListener('keydown', (event) => {
      keysPressed[event.code] = true;

      if (!(availableKeys.includes(event.code)))
        return;
  
      if (!keysPressed[controls.PlayerOneBlock]) {
        playerOneHit();
      }
      if (!keysPressed[controls.PlayerTwoBlock]) {
        playerTwoHit();
      }
      
      if(firstFighter.health === 0){
        resolve(secondFighter);
      }
      if(secondFighter.health === 0){
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (event) => {
      delete keysPressed[event.code];
    });

    function playerOneHit(): void {
      //p1 crit
      if (playerOneCanUseCrit && controls.PlayerOneCriticalHitCombination.every((x) => x in keysPressed)) {
        playerOneCanUseCrit = false;
        setTimeout(() => playerOneCanUseCrit = true, 10000);
        let damage: number = 2 * firstFighter.attack;
        changeHealth(damage, secondFighter, secondFighterIndicator, secondFighterIndicatorCoef);
      }
      //p1 hit p2 block
      else if (keysPressed[controls.PlayerOneAttack] && keysPressed[controls.PlayerTwoBlock]) {
        return;
      }
      //p1 hit
      else if (keysPressed[controls.PlayerOneAttack]) {
        let damage: number = getDamage(firstFighter, secondFighter);
        changeHealth(damage, secondFighter, secondFighterIndicator, secondFighterIndicatorCoef);
      }
    }

    function playerTwoHit(): void {
      //p2 crit
      if (playerTwoCanUseCrit && controls.PlayerTwoCriticalHitCombination.every((x) => x in keysPressed)) {
        playerTwoCanUseCrit = false;
        setTimeout(() => playerTwoCanUseCrit = true, 10000);
        let damage: number = 2 * secondFighter.attack;
        changeHealth(damage, firstFighter, firstFighterIndicator, firstFighterIndicatorCoef);
      }
      //p2 hit p1 block
      else if (keysPressed[controls.PlayerTwoAttack] && keysPressed[controls.PlayerOneBlock]) {
        return;
      }
      //p2 hit
      else if (keysPressed[controls.PlayerTwoAttack]) {
        let damage: number = getDamage(secondFighter, firstFighter);
        changeHealth(damage, firstFighter, firstFighterIndicator, firstFighterIndicatorCoef);
      }
    }

    function changeHealth(damage: number, fighter: FighterDetails, healthIndicator: HTMLElement, healthCoef: number): void{
      if(fighter.health - damage < 0){
        fighter.health = 0;
        healthIndicator.style.width = '0%';
        return;
      }
      fighter.health -= damage;
      healthIndicator.style.width = fighter.health * healthCoef + '%';
    }
    //end
  });
}

export function getDamage(attacker: FighterDetails, defender: FighterDetails): number {
  // return damage

  //start
  let damage: number = getHitPower(attacker) - getBlockPower(defender);
  return (damage < 0 ? 0 : damage);
  //end
}

export function getHitPower(fighter: FighterDetails): number {
  // return hit power

  //start
  let criticalHitChance: number = Math.random() + 1;
  let power: number = fighter.attack * criticalHitChance;
  return power;
  //end
}

export function getBlockPower(fighter: FighterDetails): number {
  // return block power

  //start
  let dodgeChance: number = Math.random() + 1;
  let power: number = fighter.defense * dodgeChance;
  return power;
  //end
}
