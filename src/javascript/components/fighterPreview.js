import { createElement } from '../helpers/domHelper';
export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });
    // todo: show fighter info (image, name, health, etc.)
    //start
    if (fighter) {
        const imageElement = createFighterImage(fighter);
        const infoElement = createElement({
            tagName: 'span',
            className: 'fighter-preview___info'
        });
        infoElement.textContent = `Health: ${fighter.health}, attack: ${fighter.attack}, defense: ${fighter.defense}`;
        fighterElement.append(infoElement);
        fighterElement.append(imageElement);
    }
    //end
    return fighterElement;
}
export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });
    return imgElement;
}
