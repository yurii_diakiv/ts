import { controls } from '../../constants/controls';
export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over
        //start
        let keysPressed = {};
        let playerOneCanUseCrit = true;
        let playerTwoCanUseCrit = true;
        let firstFighterIndicator = document.getElementById('left-fighter-indicator');
        let secondFighterIndicator = document.getElementById('right-fighter-indicator');
        const firstFighterIndicatorCoef = 100 / firstFighter.health;
        const secondFighterIndicatorCoef = 100 / secondFighter.health;
        let availableKeys = Object.values(controls).flat();
        document.addEventListener('keydown', (event) => {
            keysPressed[event.code] = true;
            if (!(availableKeys.includes(event.code)))
                return;
            if (!keysPressed[controls.PlayerOneBlock]) {
                playerOneHit();
            }
            if (!keysPressed[controls.PlayerTwoBlock]) {
                playerTwoHit();
            }
            if (firstFighter.health === 0) {
                resolve(secondFighter);
            }
            if (secondFighter.health === 0) {
                resolve(firstFighter);
            }
        });
        document.addEventListener('keyup', (event) => {
            delete keysPressed[event.code];
        });
        function playerOneHit() {
            //p1 crit
            if (playerOneCanUseCrit && controls.PlayerOneCriticalHitCombination.every((x) => x in keysPressed)) {
                playerOneCanUseCrit = false;
                setTimeout(() => playerOneCanUseCrit = true, 10000);
                let damage = 2 * firstFighter.attack;
                changeHealth(damage, secondFighter, secondFighterIndicator, secondFighterIndicatorCoef);
            }
            //p1 hit p2 block
            else if (keysPressed[controls.PlayerOneAttack] && keysPressed[controls.PlayerTwoBlock]) {
                return;
            }
            //p1 hit
            else if (keysPressed[controls.PlayerOneAttack]) {
                let damage = getDamage(firstFighter, secondFighter);
                changeHealth(damage, secondFighter, secondFighterIndicator, secondFighterIndicatorCoef);
            }
        }
        function playerTwoHit() {
            //p2 crit
            if (playerTwoCanUseCrit && controls.PlayerTwoCriticalHitCombination.every((x) => x in keysPressed)) {
                playerTwoCanUseCrit = false;
                setTimeout(() => playerTwoCanUseCrit = true, 10000);
                let damage = 2 * secondFighter.attack;
                changeHealth(damage, firstFighter, firstFighterIndicator, firstFighterIndicatorCoef);
            }
            //p2 hit p1 block
            else if (keysPressed[controls.PlayerTwoAttack] && keysPressed[controls.PlayerOneBlock]) {
                return;
            }
            //p2 hit
            else if (keysPressed[controls.PlayerTwoAttack]) {
                let damage = getDamage(secondFighter, firstFighter);
                changeHealth(damage, firstFighter, firstFighterIndicator, firstFighterIndicatorCoef);
            }
        }
        function changeHealth(damage, fighter, healthIndicator, healthCoef) {
            if (fighter.health - damage < 0) {
                fighter.health = 0;
                healthIndicator.style.width = '0%';
                return;
            }
            fighter.health -= damage;
            healthIndicator.style.width = fighter.health * healthCoef + '%';
        }
        //end
    });
}
export function getDamage(attacker, defender) {
    // return damage
    //start
    let damage = getHitPower(attacker) - getBlockPower(defender);
    return (damage < 0 ? 0 : damage);
    //end
}
export function getHitPower(fighter) {
    // return hit power
    //start
    let criticalHitChance = Math.random() + 1;
    let power = fighter.attack * criticalHitChance;
    return power;
    //end
}
export function getBlockPower(fighter) {
    // return block power
    //start
    let dodgeChance = Math.random() + 1;
    let power = fighter.defense * dodgeChance;
    return power;
    //end
}
