import FighterDetails from '../../../models/fighterDetails';
import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter: FighterDetails, position: string): HTMLElement {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  //start
  if (fighter) {
    const imageElement: HTMLElement = createFighterImage(fighter);
    const infoElement: HTMLElement = createElement ({
      tagName: 'span',
      className: 'fighter-preview___info'
    });
    infoElement.textContent = `Health: ${fighter.health}, attack: ${fighter.attack}, defense: ${fighter.defense}`;
    fighterElement.append(infoElement);
    fighterElement.append(imageElement);
  }
  //end

  return fighterElement;
}

export function createFighterImage(fighter: FighterDetails): HTMLElement {
  const { source, name }: {source: string, name: string} = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
