import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
// import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
//start
import { fighterService } from '../services/fightersService';
import FighterDetails from '../../../models/fighterDetails';
//end

export function createFightersSelector(): (event: Event, fighterId: string) => Promise<void> {
  let selectedFighters: FighterDetails[] = [];

  return async (event: Event, fighterId: string) => {
    const fighter: FighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: FighterDetails = playerOne ?? fighter;
    const secondFighter: FighterDetails = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<string, FighterDetails> = new Map();

export async function getFighterInfo(fighterId: string): Promise<FighterDetails> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap

  //start
  let fighter = fighterDetailsMap.get(fighterId);
  if (fighter) {
    return fighter;
  }
  else {
    fighter = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighter);
    return fighter;
  }
  //end
}

function renderSelectedFighters(selectedFighters: FighterDetails[]): void {
  const fightersPreview: Element = <Element>document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: FighterDetails[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = (): void => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img'
    // attributes: { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: FighterDetails[]): void {
  renderArena(selectedFighters);
}
