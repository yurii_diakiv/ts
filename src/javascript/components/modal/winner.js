//start
import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
//end
export function showWinnerModal(fighter) {
    // call showModal function
    //start
    const fighterElement = createFighterElement();
    const imageElement = createFighterImage(fighter);
    fighterElement.append(imageElement);
    showModal({ title: `Winner: ${fighter.name}`, bodyElement: fighterElement });
    //end
}
//start
function createFighterElement() {
    const fighterElement = createElement({
        tagName: 'div',
    });
    return fighterElement;
}
//end
