//start
import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import FighterDetails from '../../../../models/fighterDetails';
//end
export function showWinnerModal(fighter: FighterDetails): void {
  // call showModal function

  //start
  const fighterElement: HTMLElement = createFighterElement();
  const imageElement: HTMLElement = createFighterImage(fighter);
  fighterElement.append(imageElement);
  showModal({ title: `Winner: ${fighter.name}`, bodyElement: fighterElement });
  //end
}

//start
function createFighterElement(): HTMLElement {
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
  });
  return fighterElement;
}
//end
