interface Fighter {
    _id: string,
    name: string,
    source: string
}

export default Fighter;